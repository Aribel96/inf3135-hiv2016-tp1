# Makefile pour INF3135 / TP1

tp1: tp1.o
	gcc -o tp1 tp1.o

tp1.o: tp1.c
	gcc -c tp1.c


.PHONY: clean

.PHONY: database


clean:
	rm -f tp1 tp1.o
	rm -f *.out


database:
	curl http://download.geonames.org/export/dump/countryInfo.txt > countryInfo.txt

	curl http://download.geonames.org/export/dump/cities15000.zip > cities15000.zip

	unzip cities15000.zip
