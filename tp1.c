/**
 * La solution présentée dans ce fichier est inspirée
 * des solutions des exercices du cours qui elles meme 
 * s'inspirent en partie de la page 
 * https://en.wikipedia.org/wiki/Quicksort,
 * en utilisant la stratégie proposée par Hoare.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAILLE_MOY 10000
#define TAILLE_MIN 20
#define TAILLE_NOM 100
#define TAILLE_CODE 10
#define DEBUT_FICHIER_PAYS 3077
#define DELIMS "\t"


struct Pays {
    char nom[TAILLE_NOM];   
    char code[TAILLE_CODE]; 
};


struct Ville {
    char nom[TAILLE_NOM]; 
    long population;          
    struct Pays pays;         
};


struct Pays trouverPays(char code[],int taille, struct Pays lesPays[]);
int partitionner(struct Ville tab[], int i, int j);
void echanger(struct Ville *s, struct Ville *t);
void trierRapidementRec(struct Ville tab[], int i, int j);
void trierRapidement(struct Ville tab[], int longTab);
void afficherVilles(struct Ville tab[], int nombre, int t);
void validerUsage(int argc, char *argv[]);
int tailleFichier(char *s, int position);


int main(int argc, char *argv[]){

   validerUsage(argc, argv);

        
    FILE* source = NULL;
    FILE* fichier = NULL;

    int tailleTabVilles = tailleFichier("cities15000.txt", 0);
    int tailleTabPays = tailleFichier("countryInfo.txt", DEBUT_FICHIER_PAYS);
    
    struct Ville Villes[tailleTabVilles];
    struct Pays lesPays[tailleTabPays];
    char chaine[TAILLE_MOY] = "";    
    char s[TAILLE_MOY] = "";
    char *tab[TAILLE_MIN];
    char *tab2[TAILLE_MIN];

    char *ps, *token;
    char *pc, *pv;

 
    fichier = fopen("countryInfo.txt", "r");
 
    if (fichier != NULL)
    {
         int i;
         int j = 0;            

         fseek(fichier,DEBUT_FICHIER_PAYS, SEEK_SET);

         while( fgets(s, TAILLE_MOY, fichier) != NULL){
         
             i = 0;
             pv = s;
             while ((pc = strsep(&pv, DELIMS)) != NULL) {
 
                 tab2[i] = pc;
                 ++i;
             }
            
             struct Pays unPays;

             strcpy(unPays.nom, tab2[4]);
             strcpy(unPays.code, tab2[0]);
       
             lesPays[j] = unPays;
             ++j;
        
        }
        
        fclose(fichier);
    }
     
      

    source = fopen("cities15000.txt", "r");

    if(source != NULL)
    {   

          int i;
          int j = 0; 

          while(fgets(chaine, TAILLE_MOY, source) != NULL){
              
              i = 0;
              ps = chaine;
              while((token = strsep(&ps, DELIMS)) != NULL){
  
                  tab[i] = token;
                  ++i;
              }

                
              struct Ville uneVille;

              strcpy(uneVille.nom, tab[2]);

              int ret = atoi(tab[14]);
              uneVille.population = ret;

              struct Pays lePays = trouverPays(tab[8],tailleTabPays, lesPays);
              uneVille.pays = lePays;                

              Villes[j] = uneVille;
              ++j;
          }

             
          fclose(source);

     }

     trierRapidement(Villes, tailleTabVilles);

     char *param;
     long n = strtol(argv[1], &param, 10);

     afficherVilles(Villes, n, tailleTabVilles);    
}


/* Retourne la structure de type pays  
 * du tableau lesPays auquel code correspond
*/ 
struct Pays trouverPays(char code[],int taille, struct Pays lesPays[]){
    
    struct Pays retour;

    int i;
    for(i = 0; i < taille; ++i){
        if(strcmp(lesPays[i].code, code) == 0){
            retour = lesPays[i];
        }
    }
    
    return retour;
}

/**
* Echange les valeurs de s et t
*/
void echanger(struct Ville *s, struct Ville *t) {
    struct Ville temp;
    temp = *s;
    *s = *t;
    *t = temp;
}

/**
 * Partitionne le tableau autour de tab[i] et
 * retourne la position finale de tab[i].
 *
 * Le tableau doit contenir au moins un élément.
 */
int partitionner(struct Ville tab[], int i, int j) {
    struct Ville pivot = tab[i];
    int g = i - 1, d = j + 1;
    while (1) {
        do {
            --d;
        } while(tab[d].population > pivot.population);
        do {
            ++g;
        } while(tab[g].population < pivot.population);
        if (g < d)
            echanger(&tab[g], &tab[d]);
        else
            return d;
    }
}

/**
 * Trie récursivement le tableau donné entre les
 * indices i et j.
 *
 * Il suffit d'abord de partitionner le tableau
 * autour de tab[i]. Comme l'élément est alors
 * placé correctement, on trie récursivement les
 * deux autres portions.
 */
void trierRapidementRec(struct Ville tab[], int i, int j) {
    if (i < j) {
        int m = partitionner(tab, i, j);
        trierRapidementRec(tab, i, m);
        trierRapidementRec(tab, m + 1, j);
    }
}

/**
 * Trie rapidement le tableau donné, en appelant
 * une fonction récursive.
 */
void trierRapidement(struct Ville tab[], int longTab) {
    trierRapidementRec(tab, 0, longTab - 1);
}

/*
* Affiche nombre structures de type ville les plus populeuses  de tab
*/
void afficherVilles(struct Ville tab[], int nombre, int t){
    
    int i = t - 1;
    int n = i - nombre;

    int rang = 1;

    printf("Rang   Nom                                 Pays                                     Population\n");
    printf("----   ---                                 ----                                     ----------\n");

    while(i > n){
       
        printf("%4d   %-35s %-35s %15ld\n", rang, tab[i].nom, tab[i].pays.nom, tab[i].population);

        --i;
        ++rang;
    }
    
}

/*Valide l'usage des parametres au début du
*programme.
*/
void validerUsage(int argc, char *argv[]){

    if (argc != 2) {
        printf("Nombre d'arguments invalides\n");
        printf(" USAGE: %s <Nombre de villes>", argv[0]);
        exit(1);
    }else{
    
        char *reste;
        long nombre = strtol(argv[1], &reste, 10);

        if(reste == argv[1] || *reste != '\0'){
            printf("Le parametre passé doit être un nombre\n");
            printf(" USAGE: %s <Nombre de villes>", argv[0]);
            exit(2);
        }else if(nombre < 1 || nombre > 5000){
            printf("Le parametre passé est incorrect, veuillez entrer un entier entre 1 et 5000");
            exit(3);
        }
    }
}

/*
*Retourne le nombre de lignes dans fichier a partir de position
*/
int tailleFichier(char *s, int position){
    
    int n;
    char chaine[TAILLE_MOY] = " ";
    
    FILE* fichier = NULL;
    fichier = fopen(s , "r");
    
    fseek(fichier,position, SEEK_SET);
    
    while(fgets(chaine, TAILLE_MOY, fichier) != NULL){
        
        ++n;
    }
    fclose(fichier);
    return n;
}

    

