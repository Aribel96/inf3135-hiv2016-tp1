# Travail pratique 1

## Description

Le projet suivant est le premier travail pratique de cours de C INF3135 a la session
hivers 2016 a l'UQAM. 
Il consiste a utiliser des données géographiques sous la license CreativeCommons pour afficher la liste des villes ayant la plus grande population sur notre planète.

## Auteur

Aristide Belem (BELA16059607)

## Fonctionnement

###Voici les étapes à suivre pour compiler et exécuter le programme:###

1.make database: pour télécharger les fichiers nécéssaires
au fonctionnement du programme.

2.make: pour compiler le projet et générer l'exécutable

3.Le programme se lance en ligne de commande en tapant la commande 
./tp1 <nombre de villes>, nombre de villes étant le paramètre passé
au programme indiquant le nombre de villes à afficher. 

4.make clean: pour nettoyer le répertoire du projet en supprimant les extensions .o et l'exécutable



###Voici quelques exemples du fonctionnement du programme:###



exemple 1: Affiche les 5 villes les plus populeuses

commande: ./tp1 5,
Affichage:   

![Capture d’écran 2016-02-11 à 21.48.56.png](https://bitbucket.org/repo/broAbz/images/4277138510-Capture%20d%E2%80%99%C3%A9cran%202016-02-11%20%C3%A0%2021.48.56.png)





exemple2: Affiche les 10 villes les plus populeuses

commande: ./tp1 10,
Affichage:

![Capture d’écran 2016-02-11 à 21.44.47.png](https://bitbucket.org/repo/broAbz/images/2508003446-Capture%20d%E2%80%99%C3%A9cran%202016-02-11%20%C3%A0%2021.44.47.png)




## Contenu du projet

1.tp1.c: Contient toute l'implementation du projet

2.Makefile: Contient le code pour automatiser la compilation

3.README.md: descriptif du projet

4..gitignore: Spécification des fichiers non versionnés dans le gestionnaire de sources

## Références

1.Corrigé des exercices cours de C - inf3135, Uqam, hivers 2016

2.[Wikipédia] https://en.wikipedia.org/wiki/Quicksort

## Statut

Le projet est complété et prêt à être utilisé.